import {AppPage} from './app.po';
import {browser, by, element, logging, protractor} from 'protractor';


const openSidebar = () => {
  const hmbO = element(by.id('hamburger-open'))
  const EC = protractor.ExpectedConditions;
  browser.wait(EC.elementToBeClickable(hmbO), 5000);
  hmbO.click();
}

const logIn = () => {
  openSidebar();
  browser.sleep(1000);
  const EC = protractor.ExpectedConditions;

  const testEmail = 'e2e@tp.pl';
  const testPassword = '123456';

  const authButton = element(by.css('.auth-text'));
  browser.wait(EC.elementToBeClickable(authButton), 1000);
  authButton.click();
  browser.sleep(1000);
  element(by.id('email')).sendKeys(testEmail);
  element(by.id('password')).sendKeys(testPassword);
  const submitButton = element(by.css('.btn-submit'));
  browser.wait(EC.elementToBeClickable(submitButton), 1000);
  submitButton.click();
  browser.sleep(1000);
}

const logOut = async () => {
  openSidebar();
  browser.sleep(1000);
  const EC = protractor.ExpectedConditions;
  const text = await element(by.css('.auth-text')).getText();
  if (text === 'WYLOGUJ') {
    const authButton = element(by.css('.auth-text'));
    browser.wait(EC.elementToBeClickable(authButton), 1000);
    authButton.click();
    browser.sleep(1000);
  }
}

const navigateToUni = () => {
  const EC = protractor.ExpectedConditions;
  openSidebar();
  browser.sleep(1000);
  const uni = element(by.id('go-uni'))
  browser.wait(EC.elementToBeClickable(uni), 10000);
  uni.click();
}

const navigateToLect = async () => {
  navigateToUni();

  const EC = protractor.ExpectedConditions;
  await element.all(by.css('.select-uni'))
    .then(elems => {
      browser.wait(EC.elementToBeClickable(elems[0]), 1000);
      elems[0].click();
    });
}

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });

  it('should log in to app, navigate to lecturer page, open adding comment modal window', async () => {
    page.navigateTo();
    logIn();
    browser.sleep(1000);
    navigateToLect();
    browser.sleep(1000);
    const EC = protractor.ExpectedConditions;
    await element.all(by.css('.select-uni'))
      .then(elems => {
        browser.wait(EC.elementToBeClickable(elems[0]), 1000);
        elems[0].click();
        expect(browser.getCurrentUrl()).toMatch(/lect\/.*/);
      });
    let commAddButton = element(by.id('comment-add'));
    browser.wait(EC.elementToBeClickable(commAddButton), 1000);
    commAddButton.click();
    browser.sleep(1000);
    expect(element(by.css('.fullComment'))).toBeTruthy();
    await logOut();
  });

  it('should log in to app, navigate to lecturer page, add comment, show it and delete it', async () => {
    page.navigateTo();
    logIn();
    browser.sleep(1000);
    navigateToLect();
    browser.sleep(1000);
    const EC = protractor.ExpectedConditions;
    await element.all(by.css('.select-uni'))
      .then(elems => {
        browser.wait(EC.elementToBeClickable(elems[0]), 1000);
        elems[0].click();
        expect(browser.getCurrentUrl()).toMatch(/lect\/.*/);
      });

    let commAddButton = element(by.id('comment-add'));
    browser.wait(EC.elementToBeClickable(commAddButton), 10000);
    commAddButton.click();
    browser.sleep(1000);
    expect(element(by.css('.fullComment'))).toBeTruthy();

    let acceptComm = element(by.id('fullComment-add'));
    browser.wait(EC.elementToBeClickable(acceptComm), 10000);
    acceptComm.click();
    browser.sleep(2000);
    expect(element(by.css('.user')).getText()).toContain('E2E TEST');

    let modal = element(by.css('.comment'));
    browser.wait(EC.elementToBeClickable(modal), 10000);
    modal.click();
    browser.sleep(2000);
    expect(element(by.css('.fullComment'))).toBeTruthy();
    let deleteButton= element(by.css('.deleteButton'));
    browser.wait(EC.elementToBeClickable(deleteButton), 10000);
    deleteButton.click();
    browser.sleep(2000);
    expect(element(by.css('.user')).getText()).not.toContain('E2E TEST');
    await logOut();
  });

  it('should open and close sidebar', async () => {
    page.navigateTo();

    let width = await element(by.css('.side-bar')).getCssValue('width')
    width = width.replace("px", "");

    await element(by.css('.side-bar')).getLocation().then(location => {
      expect(location.x + +width).toBe(0);
    })

    const hmbO = element(by.id('hamburger-open'))
    const EC = protractor.ExpectedConditions;
    browser.wait(EC.elementToBeClickable(hmbO), 3000);
    hmbO.click();

    browser.sleep(500);

    await element(by.css('.side-bar')).getLocation().then(location => {
      expect(location.x + +width).toBe(+width);
    })

    browser.sleep(100);
    const hmbC = element(by.id('hamburger-close'))
    browser.wait(EC.elementToBeClickable(hmbC), 3000);
    hmbC.click();

    browser.sleep(500);

    await element(by.css('.side-bar')).getLocation().then(location => {
      expect(location.x + +width).toBe(0);
    })

  });

  it('should log in and log out', async () => {
    page.navigateTo();
    openSidebar();
    const EC = protractor.ExpectedConditions;

    const text = await element(by.css('.auth-text')).getText();

    if (text === 'WYLOGUJ') {
      const authButton = element(by.css('.auth-text'));
      browser.wait(EC.elementToBeClickable(authButton), 1000);
      authButton.click();
      browser.sleep(2000);
      expect(text).toContain('ZALOGUJ');
    }

    const testEmail = 'e2e@tp.pl';
    const testPassword = '123456';

    const authButton = element(by.css('.auth-text'));
    browser.wait(EC.elementToBeClickable(authButton), 1000);
    authButton.click();
    browser.sleep(2000);
    expect(element(by.css('.modal-title')).getText()).toContain('Logowanie');
    element(by.id('email')).sendKeys(testEmail);
    element(by.id('password')).sendKeys(testPassword);
    const submitButton = element(by.css('.btn-submit'));
    browser.wait(EC.elementToBeClickable(submitButton), 1000);
    submitButton.click();
    browser.sleep(2000);
    openSidebar();
    browser.sleep(2000);
    expect(element(by.css('.auth-text')).getText()).toContain('WYLOGUJ');
    expect(element(by.id('user-name')).getText()).toContain(`Zalogowano jako:E2E TEST`);

    browser.wait(EC.elementToBeClickable(authButton), 1000);
    authButton.click();
    browser.sleep(2000);
    openSidebar();
    browser.sleep(2000);
    expect(element(by.css('.auth-text')).getText()).toContain('ZALOGUJ');
  });

  it('should navigate to uni list from sidebar', () => {
    page.navigateTo();
    navigateToUni()
    browser.sleep(2000);
    expect(element(by.css('.header__text')).getText()).toContain('LISTA UCZELNI');
  });

  it('should navigate to favourites users unis page from sidebar after log in', async () => {
    page.navigateTo();
    logIn();
    browser.sleep(1000);
    openSidebar();
    browser.sleep(1000);
    const EC = protractor.ExpectedConditions;
    const favUni = element(by.id('gu-ulubjene'));
    browser.wait(EC.elementToBeClickable(favUni), 20000);
    favUni.click();
    browser.sleep(1000);
    expect(element(by.css('.header__text')).getText()).toContain('TWOJE ULUBIONE UCZELNIE');
    await logOut();
  });

  it('should open top unis lect page from home page', () => {
    page.navigateTo();
    const img = element(by.id('img-us'));
    const EC = protractor.ExpectedConditions;
    browser.wait(EC.elementToBeClickable(img), 1000);
    img.click();
    expect(browser.getCurrentUrl()).toMatch(/uni\/.*/);
  });

  it('should open uni list with city name filter', () => {
    page.navigateTo();

    const city = element(by.css('.element__text'));
    const EC = protractor.ExpectedConditions;
    browser.wait(EC.elementToBeClickable(city), 1000);
    city.click();
    expect(browser.getCurrentUrl()).toMatch('/uni');

    expect(element(by.id('search')).getAttribute('value')).toBe('gdańsk');

  });

  it('should display proper massage when there is no search result in Uni Table', () => {
    page.navigateTo();
    navigateToUni();

    element(by.id('search')).sendKeys('bardzo błedny tekst dla wyszukiwania');
    expect(element(by.id('no-result'))).toBeTruthy();
  });

  it("should display table elements result after correct search input e.g. 'Katowice'", () => {
    page.navigateTo();
    navigateToUni();

    const correctSearchText = 'Katowice'
    element(by.id('search')).sendKeys(correctSearchText);
    expect(element.all(by.css('tr')).count()).toBeGreaterThan(0);
  });

  it("should display less than 10 result after choosing this option from select in Uni Table", async () => {
    const EC = protractor.ExpectedConditions;

    page.navigateTo();
    navigateToUni();


    await element.all(by.tagName('option'))
      .then(options => {
        browser.wait(EC.elementToBeClickable(options[0]), 1000);
        options[0].click();
        /*One more because we have first row as table header */
        expect(element.all(by.css('tr')).count()).toBeLessThanOrEqual(11);
      });
  });

  it("should display more than 10 and less than 20 result after choosing this option from select in Uni Table", async () => {
    const EC = protractor.ExpectedConditions;

    page.navigateTo();
    navigateToUni();

    await element.all(by.tagName('option'))
      .then(options => {
        browser.wait(EC.elementToBeClickable(options[1]), 1000);
        options[1].click();
        /*One more because we have first row as table header */
        expect(element.all(by.css('tr')).count()).toBeGreaterThanOrEqual(11);
        expect(element.all(by.css('tr')).count()).toBeLessThanOrEqual(21);
      });

  });

  it("should display more than 20 and less than 50 result in  after choosing this option from select in Uni Table", async () => {
    const EC = protractor.ExpectedConditions;

    page.navigateTo();
    navigateToUni();

    await element.all(by.tagName('option'))
      .then(options => {
        browser.wait(EC.elementToBeClickable(options[2]), 1000);
        options[2].click();
        /*One more because we have first row as table header */
        expect(element.all(by.css('tr')).count()).toBeGreaterThanOrEqual(21);
        expect(element.all(by.css('tr')).count()).toBeLessThanOrEqual(51)
      });
  });

  it("should navigate to lecturers table after choosing one of Uni", async () => {
    page.navigateTo();
    navigateToUni();

    const EC = protractor.ExpectedConditions;
    await element.all(by.css('.select-uni'))
      .then(elems => {
        browser.wait(EC.elementToBeClickable(elems[0]), 1000);
        elems[0].click();

        expect(browser.getCurrentUrl()).toMatch(/uni\/.*/);
      });

  });

  it("should add, display and remove favourite uni", async () => {
    page.navigateTo();
    logIn();
    browser.sleep(1000);
    navigateToUni();
    browser.sleep(1000);
    const EC = protractor.ExpectedConditions;
    let favButton = element(by.id('heart'));
    browser.wait(EC.elementToBeClickable(favButton), 10000);
    favButton.click();
    browser.sleep(1000);
    expect(element(by.id('heart-delete'))).toBeTruthy();

    openSidebar();
    browser.sleep(3000);
    let favUni = element(by.id('gu-ulubjene'));
    browser.wait(EC.elementToBeClickable(favUni), 10000);
    favUni.click();
    browser.sleep(3000);
    expect(element.all(by.tagName('td')).count()).toBeGreaterThan(0);
    favButton = element(by.id('heart-delete'));
    browser.wait(EC.elementToBeClickable(favButton), 10000);
    favButton.click();
    browser.sleep(2000);
    expect(element(by.id('heart'))).toBeTruthy();
    browser.refresh();
    expect(element(by.id('no-result'))).toBeTruthy();
    await logOut();
  });


  it('should display proper massage when there is no search result in Lect Table', async () => {
    page.navigateTo();
    await navigateToLect();

    element(by.id('search')).sendKeys('bardzo błedny tekst dla wyszukiwania');
    expect(element(by.id('no-result'))).toBeTruthy();
  });

  it("should display table elements result after correct search input e.g. 'Wydział' in Lect Table", async () => {
    page.navigateTo();
    await navigateToLect();

    const correctSearchText = 'Wydział';
    element(by.id('search')).sendKeys(correctSearchText);
    expect(element.all(by.css('tr')).count()).toBeGreaterThan(0);
  });

  it("should display less than 10 result after choosing this option from select in Lect Table", async () => {
    const EC = protractor.ExpectedConditions;

    page.navigateTo();
    await navigateToLect();

    await element.all(by.tagName('option'))
      .then(options => {
        browser.wait(EC.elementToBeClickable(options[0]), 1000);
        options[0].click();
        /*One more because we have first row as table header */
        expect(element.all(by.css('tr')).count()).toBeLessThanOrEqual(11);
      });
  });

  it("should display less than 20 result after choosing this option from select in Lect Table", async () => {
    const EC = protractor.ExpectedConditions;

    page.navigateTo();
    await navigateToLect();

    await element.all(by.tagName('option'))
      .then(options => {
        browser.wait(EC.elementToBeClickable(options[1]), 1000);
        options[1].click();
        /*One more because we have first row as table header */
        expect(element.all(by.css('tr')).count()).toBeLessThanOrEqual(21);
      });

  });

  it("should display less than 50 result after choosing this option from select in Lect Table", async () => {
    const EC = protractor.ExpectedConditions;

    page.navigateTo();
    await navigateToLect();

    await element.all(by.tagName('option'))
      .then(options => {
        browser.wait(EC.elementToBeClickable(options[2]), 1000);
        options[2].click();
        /*One more because we have first row as table header */
        expect(element.all(by.css('tr')).count()).toBeLessThanOrEqual(51)
      });
  });

  it("should navigate to chosen lecturer rates panel after choosing one of lect from Lect Table", async () => {
    page.navigateTo();
    await navigateToLect();

    const EC = protractor.ExpectedConditions;
    await element.all(by.css('.select-uni'))
      .then(elems => {
        if (elems) {
          browser.wait(EC.elementToBeClickable(elems[0]), 1000);
          elems[0].click();
          expect(browser.getCurrentUrl()).toMatch(/lect\/.*/);
        } else {
          expect(page).toBeTruthy();
        }
      });
  });


});
