import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {UniListComponent} from './components/uni/uni-list/uni-list.component';
import {FavouriteComponent} from './components/uni/favourite/favourite.component';
import {LectListComponent} from './components/lect/lect-list/lect-list.component';
import {LectComponent} from './components/lect/lect.component';
import {TopListResolve} from './services/resolver/top-id.resolve';
import {UniResolve} from './services/resolver/uni.resolve';
import {AuthGuard} from './services/auth.guard';
import {FavResolve} from './services/resolver/fav.resolve';
import {LectListResolve} from './services/resolver/lect-list.resolve';
import {LectResolve} from './services/resolver/lect.resolve';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: {response: TopListResolve}
  },
  {
    path: 'uni',
    component: UniListComponent,
    resolve: {response: UniResolve}
  },
  {
    path: 'uni/:id',
    component: LectListComponent,
    resolve: {response: LectListResolve}
  },
  {
    path: 'lect/:id',
    component: LectComponent,
    resolve: {response: LectResolve}
  },
  {
    path: 'fav',
    component: FavouriteComponent,
    canActivate: [AuthGuard],
    resolve: {response: FavResolve}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
