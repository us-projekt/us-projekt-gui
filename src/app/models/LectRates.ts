export class LectRates {
  summaryRate: number;
  lectureRate: number;
  competenceRate: number;
  passRate: number;
  proStudentRate: number;
  activitiesRate: number;
}
