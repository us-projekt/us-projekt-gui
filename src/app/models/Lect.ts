export class Lect {
  _id: string;
  name: string;
  surname: string;
  faculty: string;
  summaryRate: number;
  lectureRate: number;
  competenceRate: number;
  passRate: number;
  proStudentRate: number;
  activitiesRate: number;
  uniId: number;
}
