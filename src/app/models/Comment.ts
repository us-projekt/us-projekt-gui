export class Comments {
  count: number;
  rates: [{
    _id: number;
    author: string;
    description: string;
    date: string;
    summaryRate: number;
    lectureRate: number;
    competenceRate: number;
    passRate: number;
    proStudentRate: number;
    activitiesRate: number;
    lectId: number;
  }];
}
