export interface Uni {
  _id?: string;
  name: string;
  city: string;
  type: string;
  isPublic: string;
}

