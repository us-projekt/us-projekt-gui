import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

import {User} from '../models/User';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class UserService {
  private readonly _SIGN_UP_URL = 'https://us-projekt.herokuapp.com/api/auth/signup';
  private readonly _LOGIN_URL = 'https://us-projekt.herokuapp.com/api/auth/login';

  logoutEvent = new BehaviorSubject<any>(false);

  private userId: string;

  constructor(private http: HttpClient,
              private router: Router) {
  }


  signUp(user: User) {
    return this.http.post<any>(this._SIGN_UP_URL, user);
  }

  logIn(user: User) {
    return this.http.post<any>(this._LOGIN_URL, user);
  }

  logOut() {
    localStorage.removeItem('TP_JWT_TOKEN');
    this.router.navigate(['/']);
  }

  logOutError() {
    this.logoutEvent.next(true);
    this.logOut();
  }

  getToken() {
    return localStorage.getItem('TP_JWT_TOKEN');
  }

  isLoggedIn() {
    return !!localStorage.getItem('TP_JWT_TOKEN');
  }

  setToken(token) {
    localStorage.setItem('TP_JWT_TOKEN', token);
  }

  setUserId(userId: string) {
    this.userId = userId;
  }

  getUserId() {
    return this.userId;
  }


}
