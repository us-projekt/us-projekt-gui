import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {UserService} from './user.service';

@Injectable()
export class UnauthorizedInterceptorService implements HttpInterceptor {

  constructor(private authService: UserService) {
  }

  intercept(req, next): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if (err?.status === 401) {
            this.authService.logOutError();
          }
          return throwError(err);
        })
      )
  }
}
