import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class LectService {

  private readonly GET_LECTS_URL = 'https://us-projekt.herokuapp.com/api/lect/byUni/';
  private readonly GET_LECT_URL = 'https://us-projekt.herokuapp.com/api/lect/';
  private readonly SEND_REQUEST = 'https://us-projekt.herokuapp.com/api/lect/';
  private readonly GET_COMMENTS = 'https://us-projekt.herokuapp.com/api/rates/';

  constructor(private http: HttpClient) {
  }

  getLects(id: string): Observable<any> {
    return this.http.get(this.GET_LECTS_URL + id);
  }

  getLect(id: string): Observable<any> {
    return this.http.get(this.GET_LECT_URL + id);
  }

  getComments(id: string): Observable<any> {
    return this.http.get(this.GET_COMMENTS + id);
  }

  sendRequest(payload) {
    return this.http.post(this.SEND_REQUEST, payload);
  }
}
