import {Injectable, PipeTransform} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {DecimalPipe} from '@angular/common';
import {debounceTime, tap, switchMap, delay} from 'rxjs/operators';
import {SortColumn, SortDirection} from '../../components/uni/uni-table/sortable.directive';
import {Uni} from '../../models/Uni';

interface SearchResult {
  uni: [];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn;
  sortDirection: SortDirection;
}

const compare = (v1: string, v2: string) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function sort(countries: Uni[], column: SortColumn, direction: string): Uni[] {
  if (direction === '' || column === '') {
    return countries;
  } else {
    return [...countries].sort((a, b) => {
      const res = compare(`${a[column]}`, `${b[column]}`);
      return direction === 'asc' ? res : -res;
    });
  }
}

function matches(uni: Uni, term: string, pipe: PipeTransform) {
  return uni.name.toLowerCase().includes(term.toLowerCase()) ||
    uni.city.toLowerCase().includes(term.toLowerCase()) ||
    uni.type.toLowerCase().includes(term.toLowerCase()) ||
    uni.isPublic.toLowerCase().includes(term.toLowerCase());
}


@Injectable({providedIn: 'root'})
export class UniTableService {
  private loading = new BehaviorSubject<boolean>(true);
  private search = new Subject<void>();
  private uni = new BehaviorSubject<Uni[]>([]);
  private total = new BehaviorSubject<number>(0);

  unis;

  state: State = {
    page: 1,
    pageSize: 10,
    searchTerm: '',
    sortColumn: '',
    sortDirection: ''
  };

  constructor(private pipe: DecimalPipe) {
    this.search.pipe(
      tap(() => this.loading.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this.loading.next(false))
    ).subscribe(result => {
      this.uni.next(result.uni);
      this.total.next(result.total);
    });

    this.search.next();
  }

  get uni$() {
    return this.uni.asObservable();
  }

  get total$() {
    return this.total.asObservable();
  }

  get loading$() {
    return this.loading.asObservable();
  }

  get page() {
    return this.state.page;
  }

  get pageSize() {
    return this.state.pageSize;
  }

  get searchTerm() {
    return this.state.searchTerm;
  }

  set page(page: number) {
    this._set({page});
  }

  set pageSize(pageSize: number) {
    this._set({pageSize});
  }

  set searchTerm(searchTerm: string) {
    this._set({searchTerm});
  }

  set sortColumn(sortColumn: SortColumn) {
    this._set({sortColumn});
  }

  set sortDirection(sortDirection: SortDirection) {
    this._set({sortDirection});
  }

  private _set(patch: Partial<State>) {
    Object.assign(this.state, patch);
    this.search.next();
  }

  private _search(): Observable<SearchResult> {
    const {sortColumn, sortDirection, pageSize, page, searchTerm} = this.state;

    // 1. sort
    let uni = sort(this.unis, sortColumn, sortDirection);

    // 2. filter
    uni = uni.filter(country => matches(country, searchTerm, this.pipe));
    const total = uni.length;
    // 3. paginate
    uni = uni.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    // @ts-ignore
    return of({uni, total});
  }


}
