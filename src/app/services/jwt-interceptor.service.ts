import {Injectable, Injector} from '@angular/core';
import {HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserService} from './user.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector) {
  }

  intercept(req, next): Observable<HttpEvent<any>> {
    const authService = this.injector.get(UserService);
    const tokenizedReq = req.clone({
      setHeaders: {Authorization: `bearer ${authService.getToken()}`}
    });
    return next.handle(tokenizedReq);
  }
}
