import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Comments} from "../models/Comment";

@Injectable({
  providedIn: 'root'
})
export class LectRateService {
  constructor(private http: HttpClient) {
  }

  getCommentById(id) {
    return this.http.get<Comments>('https://us-projekt.herokuapp.com/api/rate/' + id);
  }

  postNewComment(comment) {
    return this.http.post('https://us-projekt.herokuapp.com/api/rate', comment);
  }

  checkUserId(userId, commentId) {
    return this.http.get<{ userId: boolean }>('https://us-projekt.herokuapp.com/api/rate/' + userId + '/' + commentId);
  }

  deleteComment(commentId) {
    return this.http.delete('https://us-projekt.herokuapp.com/api/rate/' + commentId);
  }

  patchLect(lectId, value) {
    return this.http.patch('https://us-projekt.herokuapp.com/api/lect/' + lectId, [{
      "propName": "summaryRate",
      "value": value
    }]);
  }

  isDoubledComment(userId, lectId) {
    return this.http.get<{ status: boolean }>('https://us-projekt.herokuapp.com/api/rate/check/' + lectId + '/' + userId);
  }

}
