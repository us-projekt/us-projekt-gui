import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {UniService} from '../uni.service';

@Injectable()
export class UniResolve implements Resolve<Observable<any>> {
  constructor(private uniService: UniService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.uniService.getAllUnis();
  }
}
