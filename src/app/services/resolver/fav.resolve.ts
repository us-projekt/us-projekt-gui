import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {UniService} from '../uni.service';
import * as jwtDecode from 'jwt-decode';
import {UserService} from '../user.service';

@Injectable()
export class FavResolve implements Resolve<Observable<any>> {
  constructor(private uniService: UniService,
              private  userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    const token = this.userService.getToken();
    const userId = jwtDecode(token)._id;
    return this.uniService.getFav(userId);
  }
}
