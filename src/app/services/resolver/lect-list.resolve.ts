import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {LectService} from '../lect.service';


@Injectable()
export class LectListResolve implements Resolve<Observable<any>> {
  constructor(private lectService: LectService,
              private route: ActivatedRoute) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.lectService.getLects(route.params.id);
  }
}
