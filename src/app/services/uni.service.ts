import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class UniService {

  private readonly GET_ALL_URL ='https://us-projekt.herokuapp.com/api/uni';
  private readonly GET_IDS_URL = `${this.GET_ALL_URL}/topId`;
  private readonly SET_FAV = `${this.GET_ALL_URL}/fav/`;

  constructor(private http: HttpClient) {
  }

  getTopIds() {
    return this.http.get(this.GET_IDS_URL);
  }

  getAllUnis() {
    return this.http.get(this.GET_ALL_URL);
  }

  getFav(userId: string) {
    return this.http.get(this.SET_FAV + userId);
  }

  updateFav(userId, data) {
    const payload = {favouritesUniId: data};
    return this.http.patch(this.SET_FAV + userId, payload);
  }

}
