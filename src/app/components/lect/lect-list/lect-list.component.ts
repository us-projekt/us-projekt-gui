import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-lect-list',
  template: `
    <app-header [firstLineText]="'LISTA PROWADZĄCYCH - '+ nazwaUczelni | uppercase"></app-header>
    <app-lect-table [nazwaUczelni]="nazwaUczelni" [lects]="lectList"></app-lect-table>
  `,
})
export class LectListComponent implements OnInit {
  nazwaUczelni: string;
  lectList;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.nazwaUczelni = this.route.snapshot.data.response.name;
    this.lectList = this.route.snapshot.data.response.lects;
  }

}
