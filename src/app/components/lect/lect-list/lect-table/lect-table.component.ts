import {Component, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Observable} from 'rxjs';
import {NgbdSortableLectHeader, SortEvent} from './sortable-lect.directive';
import {DecimalPipe} from '@angular/common';
import {faChevronDown, faChevronLeft, faChevronUp, faPlus} from '@fortawesome/free-solid-svg-icons';
import {ActivatedRoute, Router} from '@angular/router';
import {faHandPointer, faHeart} from '@fortawesome/free-regular-svg-icons';
import {LectTableService} from '../../../../services/table/lect-table.service';
import {Lect} from '../../../../models/Lect';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {LectService} from '../../../../services/lect.service';
import {UserService} from '../../../../services/user.service';

@Component({
  selector: 'app-lect-table',
  templateUrl: './lect-table.component.html',
  styleUrls: ['./lect-table.component.scss'],
  providers: [LectTableService, DecimalPipe]
})
export class LectTableComponent implements OnInit {
  @Input() lects;

  lect$: Observable<Lect[]>;
  total$: Observable<number>;


  lectForm;
  @Input() nazwaUczelni;

  @ViewChildren(NgbdSortableLectHeader) headers: QueryList<NgbdSortableLectHeader>;
  arrowUp = faChevronUp;
  arrowDown = faChevronDown;
  selectIcon = faHandPointer;
  warning = false;
  success = false;

  constructor(public service: LectTableService,
              private router: Router,
              private route: ActivatedRoute,
              public  modalService: NgbModal,
              private fb: FormBuilder,
              private lectService: LectService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.service.lects = this.lects;
    this.lect$ = this.service.lect$;
    this.total$ = this.service.total$;

    this.lectForm = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      faculty: ['', Validators.required],
      uniId: [this.route.snapshot.params.id, Validators.required]
    })


  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  navigateToUni(id: string) {
    this.router.navigate(['/lect/' + id]);
  }

  getColor = (summaryRate: number) => !summaryRate ? 'black' : summaryRate < 3 ? '#c82333' : '#28a745';

  sendRequest() {
    let payload = this.lectForm.value;
    payload.authorId = this.userService.getUserId();
    this.lectService.sendRequest(payload)
      .subscribe(() => {
        this.modalService.dismissAll();
        this.success = true;
        setTimeout(() => {
          this.success = false;
        }, 3000)
      });
  }

  openModal(content) {
    this.userService.isLoggedIn() ? this.modalService.open(content) : this.showWarning();
  }

  private showWarning() {
    this.warning = true;
    setTimeout(() => {
      this.warning = false;
    }, 5000)
  }
}

