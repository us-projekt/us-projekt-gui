import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Lect} from '../../models/Lect';
import * as _ from 'lodash';
import {LectRates} from '../../models/LectRates';
import {LectRateService} from '../../services/lectRate.service';
import {UserService} from '../../services/user.service';
import * as jwtDecode from 'jwt-decode';

@Component({
  selector: 'app-lect',
  templateUrl: './lect.component.html',
  styleUrls: ['./lect.component.scss']
})
export class LectComponent implements OnInit {
  newComment: FormGroup;
  private lectId;
  userId = 0;
  userName = 'TEST';
  currentDate;
  private mark = 0;
  doubledComment = false;
  lectData: Lect;
  percent = 0;
  textMark = 'ndst';
  elements = [
    {
      rate: 0,
      name: 'summaryRate',
      label: 'Ocena zajęć',
    },
    {
      rate: 0,
      name: 'lectureRate',
      label: 'Kompetencja'
    },
    {
      rate: 0,
      name: 'competenceRate',
      label: 'Zaliczenia',
    },
    {
      rate: 0,
      name: 'proStudentRate',
      label: 'Prostudencki',
    },
    {
      rate: 0,
      name: 'activitiesRate',
      label: 'Zajęcia dodatkowe',
    },
  ];
  commentRate = [
    {
      name: 'Ocena zajęć',
      mark: 0
    },
    {
      name: 'Kompetencja',
      mark: 0
    },
    {
      name: 'Zaliczenia',
      mark: 0
    },
    {
      name: 'Prostudencki',
      mark: 0
    },
    {
      name: 'Zajęcia dodatkowe',
      mark: 0
    },
  ];
  activeComments = [];
  canRight = false;
  canLeft = false;
  pointer = 0;
  page = 1;
  pagination = 3;
  comments = [];
  myRate = [
    {
      name: 'Ocena zajęć',
      rate: 1
    },
    {
      name: 'Kompetencja',
      rate: 1
    },
    {
      name: 'Zaliczenia',
      rate: 1
    },
    {
      name: 'Prostudencki',
      rate: 1
    },
    {
      name: 'Zajęcia dodatkowe',
      rate: 1
    },
  ];
  fullComment;
  isCommentOpen = false;
  isAddCommentOpen = false;
  lectRates: LectRates;
  private summaryRate = 0;
  private lectureRate = 0;
  private competenceRate = 0;
  private passRate = 0;
  private proStudentRate = 0;
  private activitiesRate = 0;
  private commentCounter = 0;
  deleteStatus = false;
  user = {
    name: '',
    surname: '',
    id: ''
  };
  isLoggedIn;

  constructor(private route: ActivatedRoute,
              private fb: FormBuilder,
              private rateService: LectRateService,
              private router: Router,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.lectId = this.route.snapshot.params.id;
    this.getComments();
    this.lectData = this.route.snapshot.data.response.lect;
    this.lectRates = _.omit(this.lectData, ['_id', 'name', 'surname', 'faculty', 'uniId']);
    this.isLoggedIn = this.userService.isLoggedIn();
    if (this.isLoggedIn) {
      const token = this.userService.getToken();
      this.userName = jwtDecode(token).name + ' ' + jwtDecode(token).surname;
      this.userId = jwtDecode(token)._id;
    }
    // create empty form for comment
    this.createForm();
    this.isDoubledComment();
  }

  previousComments() {
    if (this.comments[this.pointer - this.pagination]) {
      this.activeComments = [];
      this.pointer = this.pointer - this.pagination;
      this.page--;
      this.loadComments();
    }
  }

  isDoubledComment() {
    this.rateService.isDoubledComment(this.userId, this.lectId).subscribe(
      data => {
        this.doubledComment = data.status;
      });
  }

  nextComments() {
    if (this.comments[this.pointer + this.pagination]) {
      this.activeComments = [];
      this.pointer = this.pointer + this.pagination;
      this.page++;
      this.loadComments();
    }
  }

  checkPossibility() {
    this.canRight = !!this.comments[this.pointer + this.pagination];
    this.canLeft = !!this.comments[this.pointer - this.pagination];
  }

  loadComments() {
    for (let i = this.pointer; i < this.pointer + 3; i++) {
      if (this.comments[i]) {
        this.activeComments.push(this.comments[i]);
      }
    }
    this.checkPossibility();
    this.isDoubledComment();
  }

  setMainStars() {
    this.commentCounter = 0;
    this.lectureRate = 0;
    this.competenceRate = 0;
    this.passRate = 0;
    this.proStudentRate = 0;
    this.activitiesRate = 0;
    // zliczenie wszystkich ocen z komentow i wyliczenie sredniej
    for (const ele of this.comments) {
      this.commentCounter++;
      this.lectureRate += ele.marks[0];
      this.competenceRate += ele.marks[1];
      this.passRate += ele.marks[2];
      this.proStudentRate += ele.marks[3];
      this.activitiesRate += ele.marks[4];
    }
    this.elements[0].rate = this.lectureRate / this.commentCounter;
    this.elements[1].rate = this.competenceRate / this.commentCounter;
    this.elements[2].rate = this.passRate / this.commentCounter;
    this.elements[3].rate = this.proStudentRate / this.commentCounter;
    this.elements[4].rate = this.activitiesRate / this.commentCounter;
    this.summaryRate = _.meanBy(this.elements, (r) => r.rate);
    for (let i = 0; i < 5; i++) {
      this.elements[i].rate = Math.floor(this.elements[i].rate);
    }
  }

  changeRate(which, stars) {
    this.myRate[which].rate = stars + 1;
  }

  createDate() {
    let day;
    let month;
    const year = new Date().getFullYear();
    new Date().getDay() < 10 ? day = '0' + new Date().getDay() : day = new Date().getDay();
    (new Date().getMonth() + 1) < 10 ? month = '0' + (new Date().getMonth() + 1) : month = (new Date().getMonth() + 1);
    return year + '-' + month + '-' + day;
  }

  mainMark() {
    if (this.mark === 0) {
      this.textMark = 'Ndst';
      return 'star-00';
    } else if (this.mark >= 0 && this.mark <= 2) {
      this.textMark = 'Ndst';
      return 'star-02';
    } else if (this.mark > 2 && this.mark <= 3) {
      this.textMark = 'Dostateczny';
      return 'star-05';
    } else if (this.mark > 3 && this.mark <= 4) {
      this.textMark = 'Dobry';
      return 'star-07';
    } else if (this.mark > 4 && this.mark <= 5) {
      this.textMark = 'Bardzo dobry';
      return '';
    }
  }

  setMarks() {
    this.summaryRate ? this.mark = this.summaryRate : this.mark = 0;
    this.percent = Math.ceil(this.mark * 20);
  }

  openComponent(comment) {
    this.isCommentOpen = true;
    this.fullComment = comment;
    if (this.isLoggedIn) {
      this.rateService.checkUserId(this.userId, comment.id).subscribe(
        data => {
          this.deleteStatus = data.userId;
        },
        error => {
        },
        () => {
          return status;
        }
      );
    }
  }

  closeComponent() {
    this.isCommentOpen = false;
    this.deleteStatus = false;
  }

  createArrayForYellowStars(n: number): any[] {
    if (n) {
      return Array(n);
    } else {
      return Array(0);
    }
  }

  createArrayForGreyStars(n: number): any[] {

    if (n) {
      return Array(5 - n);
    } else {
      return Array(5);
    }
  }

  myAverage(el): number {
    let numberToReturn = 0;
    for (const element of el) {
      numberToReturn += element;
    }
    return numberToReturn / el.length;
  }

  deleteComment(comment) {
    // close current component
    this.closeComponent();
    // delete comment from local array with objects
    this.activeComments = this.activeComments.filter(myComments => myComments !== comment);
    this.rateService.deleteComment(comment.id).subscribe(
      data => {

      },
      error => {
      },
      () => {
        this.comments = [];
        this.getComments();
        this.activeComments = [];
        this.loadComments();
      }
    );
    // send DELETE method to server
  }

  createForm() {
    this.newComment = this.fb.group({
      author: [],
      description: [null, Validators.required],
      date: [],
      summaryRate: [this.myRate, Validators.required],
      lectureRate: [null, Validators.required],
      competenceRate: [null, Validators.required],
      passRate: [null, Validators.required],
      proStudentRate: [null, Validators.required],
      activitiesRate: [null, Validators.required],
      lectId: []
    });
  }

  sendComment() {
    // create new object-comment
    let commentToSend;
    // prepare new comment
    commentToSend = {
      author: this.userName,
      userId: this.userId,
      summaryRate: _.meanBy(this.myRate, (r) => r.rate),
      lectureRate: this.myRate[0].rate,
      competenceRate: this.myRate[1].rate,
      passRate: this.myRate[2].rate,
      proStudentRate: this.myRate[3].rate,
      activitiesRate: this.myRate[4].rate,
      date: this.createDate(),
      description: (this.newComment.get('description').value === null || this.newComment.get('description').value.trim() === '') ? 'Brak komentarza' : this.newComment.get('description').value,
      lectId: this.lectId
    };
    this.comments.push(commentToSend);
    this.rateService.postNewComment(commentToSend).subscribe(
      data => {
      },
      error => {

      },
      () => {
        this.comments = [];
        this.activeComments = [];
        this.getComments();
      }
    );

    // close component
    this.isAddCommentOpen = !this.isAddCommentOpen;
  }

  getComments() {
    const tempArray = [];
    this.rateService.getCommentById(this.lectId).subscribe(
      data => {
        for (const elem of data.rates) {
          tempArray.push({
            id: elem._id,
            name: elem.author,
            marks: [
              elem.lectureRate,
              elem.competenceRate,
              elem.passRate,
              elem.proStudentRate,
              elem.activitiesRate
            ],
            date: elem.date,
            comment: elem.description
          });
        }
      },
      err => {
      },
      () => {
        this.comments = tempArray.reverse();
        this.setMainStars();
        this.setMarks();
        this.loadComments();
        this.rateService.patchLect(this.lectId, this.summaryRate).subscribe();
      }
    );

  }

}
