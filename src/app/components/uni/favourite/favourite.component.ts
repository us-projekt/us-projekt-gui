import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-favourite',
  template: `
    <app-header [firstLineText]="'TWOJE ULUBIONE UCZELNIE'">
    </app-header>
    <app-uni-table [unis]="unis"></app-uni-table>
  `
})
export class FavouriteComponent implements OnInit {
  unis = [];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.unis = this.route.snapshot.data.response.unis;
  }
}
