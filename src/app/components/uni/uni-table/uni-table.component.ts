import {Component, Renderer2, Input, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Uni} from '../../../models/Uni';
import {Observable} from 'rxjs';
import {NgbdSortableHeader, SortEvent} from './sortable.directive';
import {UniTableService} from '../../../services/table/uni-table.service';
import {DecimalPipe} from '@angular/common';
import {faChevronDown, faChevronUp, faHeart as faHeartChecked} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {faHandPointer, faHeart} from '@fortawesome/free-regular-svg-icons';
import {UserService} from '../../../services/user.service';
import {UniService} from '../../../services/uni.service';
import * as jwtDecode from 'jwt-decode';

@Component({
  selector: 'app-uni-table',
  templateUrl: './uni-table.component.html',
  styleUrls: ['./uni-table.component.scss'],
  providers: [UniTableService, DecimalPipe]
})
export class UniTableComponent implements OnInit {
  uni$: Observable<Uni[]>;
  total$: Observable<number>;


  @Input() city;
  @Input() unis;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  arrowUp = faChevronUp;
  arrowDown = faChevronDown;
  selectIcon = faHandPointer;
  favouriteIcon = faHeart;
  favouriteIconChecked = faHeartChecked;

  isLoggedIn: boolean;
  userId: string;
  private currentFav = [];

  constructor(public service: UniTableService,
              private router: Router,
              private  render: Renderer2,
              private authService: UserService,
              private uniService: UniService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    if (this.isLoggedIn) {
      const token = this.authService.getToken();
      this.userId = jwtDecode(token)._id;
      this.uniService.getFav(this.userId).subscribe(res => {
        // @ts-ignore
        this.currentFav = res.unis;
      });
    }

    this.service.unis = this.unis;
    this.uni$ = this.service.uni$;
    this.total$ = this.service.total$;

    if (this.city) {
      this.service.state.searchTerm = this.city;
    }
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  navigateToUni(id: string) {
    this.router.navigate(['/uni/' + id]);
  }

  addToFav(uniId, element) {
    this.currentFav.push(this.unis.find(uni => uni._id === uniId));
    this.render.addClass(element, 'heart-Animation');
    this.uniService.updateFav(this.userId, this.currentFav).subscribe(() => {

    });
  }

  isFav(uniId) {
    return !!this.currentFav.find(uni => uni._id === uniId);
  }

  removeFormFav(uniId, element) {
    const index = this.currentFav.findIndex(uni => uni._id === uniId);
    this.currentFav.splice(index, 1);
    this.render.removeClass(element, 'heart-Animation');
    this.uniService.updateFav(this.userId, this.currentFav).subscribe(() => {
    });
  }
}

