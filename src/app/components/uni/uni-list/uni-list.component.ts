import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-uni-list',
  template: `
    <app-header [firstLineText]="'LISTA UCZELNI'">
    </app-header>
    <app-uni-table [city]="city"
                   [unis]="unis">
    </app-uni-table>`
})
export class UniListComponent {

  city = this.router.getCurrentNavigation().extras?.state?.city;
  unis;

  constructor(private router: Router, private route: ActivatedRoute) {
    this.unis = this.route.snapshot.data.response.unis;
  }

}
