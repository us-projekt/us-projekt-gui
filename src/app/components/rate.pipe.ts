import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'rate'})
export class RatePipe implements PipeTransform {
  transform = (value: number): string => value > 0 ? ((value - Math.floor(value)) === 0 ? value.toString() + '.0' : value.toFixed(1).toString()) : 'NIE OCENIONO';
}
