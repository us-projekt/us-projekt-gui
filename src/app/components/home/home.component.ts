import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  cities = [
    'gdańsk',
    'Katowice',
    'Kraków',
    'Wrocław',
    'Warszawa'
  ];

  ids;
  showError = false;

  constructor(private router: Router, private route: ActivatedRoute, private authService: UserService) {
    this.ids = this.route.snapshot.data.response.ids;
  }

  ngOnInit(): void {
    this.authService.logoutEvent
      .subscribe(res => {
        if (res) {
          this.showError = true;
        }
        setTimeout(() => {
          this.showError = false;
        }, 10000)
      })

  }

  select(element: string) {
    this.router.navigate(['/uni'], {state: {city: element}});
  }
}
