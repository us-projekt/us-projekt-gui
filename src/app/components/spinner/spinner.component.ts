import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `
    <div class="text-center {{marginTop}}">
      <div [ngClass]="{'spinner-big': isBig, 'spinner-small': !isBig}" class="spinner-grow text-info" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
  `,
  styles: [`
    .spinner-big {
      width: 6rem;
      height: 6rem;
    }

    .spinner-small {
      width: 3rem;
      height: 3rem;
    }
  `]
})
export class SpinnerComponent {
  @Input() isBig = false;
  @Input() marginTop = 'mt-3';
}
