import {Component, OnInit} from '@angular/core';
import {faBars, faUser} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import * as jwtDecode from 'jwt-decode';
import {User} from '../../models/User';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  hamburgerIcon = faBars;
  userIcon = faUser;

  isOpen = false;
  isLoggedIn;
  isRegisterMode = false;
  isLoading = false;

  user = {
    name: '',
    surname: ''
  };
  error = '';

  form: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(5)]]
  });


  constructor(private router: Router,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private authService: UserService) {
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(res => {
      this.error = null;
      this.form.get('password')?.setErrors(null);
      this.form.get('passwordConfirm')?.setErrors(null);
    });

    this.authService.logoutEvent
      .subscribe(res => {
        if (res) {
          this.isLoggedIn = false;
        }
      })

    this.isLoggedIn = this.authService.isLoggedIn();
    if (this.isLoggedIn) {
      const token = this.authService.getToken();
      this.user.name = jwtDecode(token).name;
      this.user.surname = jwtDecode(token).surname;
    }
  }

  toggleNavbar() {
    this.isOpen = !this.isOpen;
  }

  openModal(registration: boolean = false, content) {
    if (this.isLoggedIn) {
      // this.logOut();
      return;
    }

    this.isRegisterMode = registration;
    this.form.reset();
    if (!this.isRegisterMode) {
      this.form.removeControl('name');
      this.form.removeControl('surname');
      this.form.removeControl('passwordConfirm');
    } else {
      this.form.addControl('name', this.formBuilder.control('', [Validators.required, Validators.maxLength(20), Validators.minLength(2)]));
      this.form.addControl('surname', this.formBuilder.control('',
        [Validators.required, Validators.maxLength(20), Validators.minLength(2)]));
      this.form.addControl('passwordConfirm', this.formBuilder.control('', [Validators.required, Validators.minLength(5)]));
    }
    this.modalService.open(content);
  }

  navigateTo(path: string) {
    this.router.navigate(['/' + path])
      .then(() => this.isOpen = false);
  }

  submit(modal) {
    this.isRegisterMode ? this.signUp(modal) : this.logIn(modal);
  }

  private logIn(modal) {
    this.isLoading = true;
    const user: User = {
      email: this.form.get('email').value,
      password: this.form.get('password').value
    };
    this.authService.logIn(user)
      .subscribe(res => {
          this.authService.setToken(res.token);
          const userId = jwtDecode(res.token)._id;
          this.user.name = jwtDecode(res.token).name;
          this.user.surname = jwtDecode(res.token).surname;
          this.authService.setUserId(userId);
          this.isLoggedIn = true;
          modal.close();
          this.router.navigate(['/']);
          this.isOpen = false;
        },
        res => {
          this.handleErrors(res);
          this.isLoading = false;
        });
  }

  private signUp(modal) {
    this.isLoading = true;
    if (this.form.get('passwordConfirm').value !== this.form.get('password').value) {
      this.error = 'Podane hasła się różnią';
      this.form.get('password').setErrors({error: 'Hasła nie zgadzają się'});
      this.form.get('passwordConfirm').setErrors({error: 'Hasła nie zgadzają się'});
      this.isLoading = false;
      return;
    }
    const user: User = {
      name: this.form.get('name').value,
      surname: this.form.get('surname').value,
      email: this.form.get('email').value,
      password: this.form.get('password').value
    };
    this.authService.signUp(user)
      .subscribe(res => {
        this.isLoading = false;
        this.isLoggedIn = true;
        this.authService.setToken(res.token);
        const userId = jwtDecode(res.token)._id;
        this.user.name = jwtDecode(res.token).name;
        this.user.surname = jwtDecode(res.token).surname;
        this.authService.setUserId(userId);
        this.isLoggedIn = true;
        modal.close();
        this.router.navigate(['/']);
        this.isOpen = false;
      }, res => this.handleErrors(res));

  }

  private handleErrors(response) {
    this.isLoading = false;

    if (response?.error?.massage) {
      this.error = response.error.massage;
    }

    if (!this.error) {
      this.error = 'Wystąpił nieoczekiwany błąd';
    }
  }

  logOut() {
    this.authService.logOut();
    this.isLoggedIn = false;
    this.isOpen = false;
  }
}
