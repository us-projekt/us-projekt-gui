import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FavouriteComponent} from './components/uni/favourite/favourite.component';
import {UniListComponent} from './components/uni/uni-list/uni-list.component';
import {HomeComponent} from './components/home/home.component';
import {HeaderComponent} from './components/header/header.component';
import {UniTableComponent} from './components/uni/uni-table/uni-table.component';
import {NgbdSortableHeader} from './components/uni/uni-table/sortable.directive';
import {LectListComponent} from './components/lect/lect-list/lect-list.component';
import {NgbdSortableLectHeader} from './components/lect/lect-list/lect-table/sortable-lect.directive';
import {LectTableComponent} from './components/lect/lect-list/lect-table/lect-table.component';
import {LectComponent} from './components/lect/lect.component';
import {TopListResolve} from './services/resolver/top-id.resolve';
import {UniResolve} from './services/resolver/uni.resolve';
import {FavResolve} from './services/resolver/fav.resolve';
import {TokenInterceptorService} from './services/jwt-interceptor.service';
import {LectListResolve} from "./services/resolver/lect-list.resolve";
import { SpinnerComponent } from './components/spinner/spinner.component';
import { RatePipe } from './components/rate.pipe';
import {LectResolve} from './services/resolver/lect.resolve';
import {CommentsResolve} from './services/resolver/comments.resolve';
import {UnauthorizedInterceptorService} from './services/unauthorized-interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    FavouriteComponent,
    UniListComponent,
    HomeComponent,
    HeaderComponent,
    UniTableComponent,
    NgbdSortableHeader,
    NgbdSortableLectHeader,
    LectListComponent,
    LectTableComponent,
    LectComponent,
    SpinnerComponent,
    RatePipe
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    TopListResolve,
    UniResolve,
    FavResolve,
    LectListResolve,
    LectResolve,
    CommentsResolve,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
